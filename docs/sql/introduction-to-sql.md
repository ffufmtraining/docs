---
sidebar_position: 0
---

# Introduction to SQL

SQL or **Structured Query Language** is a standard programming language specifically designed for storing, retrieving, managing or manipulating the data inside a relational database management system (RDBMS). It is the most widely-implemented database language developed in the 1970s and was initially called **SEQUEL** or Structured English Query Language which was later changed to SQL. 

:::info Note
Some features of the SQL standard are implemented differently in different database systems.
:::

## Things you can do with SQL

✔️ Create a database. <br/>
✔️ Create tables in a database. <br/>
✔️ Query or request information from a database. <br/>
✔️ Insert records in a database. <br/>
✔️ Update or modify records in a database. <br/>
✔️ Delete records from the database. <br/>
✔️ Set permissions or access control within the database for data security. <br/>
✔️ Create views to avoid typing frequently used complex queries. <br/>

:::info Note
Most of relational database systems also have their own additional proprietary extensions in addition to the SQL standard that are available only on their system.
:::

## Why SQL?

📌 Allows users to access data in the relational database management systems. <br/>
📌 Allows users to describe the data. <br/>
📌 Allows users to define the data in a database and manipulate that data.<br/>
📌 Allows to embed within other languages using SQL modules, libraries & pre-compilers.<br/>
📌 Allows users to create and drop databases and tables.<br/>
📌 Allows users to create view, stored procedure, functions in a database.<br/>
📌 Allows users to set permissions on tables, procedures and views.<br/>

## SQL Process

When you are executing an SQL command for any RDBMS, the system determines the best way to carry out your request and SQL engine figures out how to interpret the task.

There are various components included in this process: 

1. Query Dispatcher
2. Optimization Engines
3. Classic Query Engine
4. SQL Query Engine, etc.

:::info Note 
A classic query engine handles all the non-SQL queries, but a SQL query engine won't handle logical files.
:::

## SQL Architecture

Here is a simple diagram showing the SQL Architecture:

![SQL Diagram](/img/diagram.png "SQL Diagram")

## SQL Commands

The standard SQL commands to interact with relational databases are `CREATE`, `SELECT`, `INSERT`, `UPDATE`, `DELETE` and `DROP`. These commands can be classified into the following groups based on their nature:

### Data Definition Language

|             Command              |                    Description                                                       | 
|:--------------------------------:|:------------------------------------------------------------------------------------:|         
|      CREATE                      |           Creates a new table, a view of a table, or other object in the database    |
|     ALTER                        |          Modifies an existing database object, such as a table                       |   
|         DROP                     |          Deletes an entire table, a view of a table or other objects in the database |

### Data Manipulation Language

|             Command              |                    Description                               | 
|:--------------------------------:|:------------------------------------------------------------:|         
|     SELECT                       |           Retrieves certain records from one or more tables  |
|     INSERT                       |         Creates a record                                     |   
|     UPDATE                       |         Modifies records                                     |
|     DELETE                       |           Deletes records                                    |     

### Data Control Language

|             Command              |                    Description                               | 
|:--------------------------------:|:------------------------------------------------------------:|         
|    GRANT                         |           Gives a privilege to user                          |
|    REVOKE                        |        Takes back privileges granted from user               |   















