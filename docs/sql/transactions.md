---
sidebar_position: 27
---

# Transactions

A transaction is a unit of work that is performed against a database. Transactions are units or sequences of work accomplished in a logical order, whether in a manual fashion by a user or automatically by some sort of a database program.

A transaction is the propagation of one or more changes to the database. For example, if you are creating a record or updating a record or deleting a record from the table, then you are performing a transaction on that table. It is important to control these transactions to ensure the data integrity and to handle database errors.

Practically, you will club many SQL queries into a group and you will execute all of them together as a part of a transaction.

## Properties 

Transactions have the following four standard properties, usually referred to by the acronym **ACID**.

- **Atomicity** − ensures that all operations within the work unit are completed successfully. Otherwise, the transaction is aborted at the point of failure and all the previous operations are rolled back to their former state.

- **Consistency** − ensures that the database properly changes states upon a successfully committed transaction.

- **Isolation** − enables transactions to operate independently of and transparent to each other.

- **Durability** − ensures that the result or effect of a committed transaction persists in case of a system failure.

## Transaction Control

The following commands are used to control transactions:

- **COMMIT** − to save the changes.

- **ROLLBACK** − to roll back the changes.

- **SAVEPOINT** − creates points within the groups of transactions in which to ROLLBACK.

- **SET TRANSACTION** − Places a name on a transaction.

## DEMO Database

Below is an example table named "CUSTOMERS":

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  1 | Ramesh   |  32 | Ahmedabad |  2000.00 |
|  2 | Khilan   |  25 | Delhi     |  1500.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  4 | Chaitali |  25 | Mumbai    |  6500.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |

### Examples

#### COMMIT

The `COMMIT` command is the transactional command used to save changes invoked by a transaction to the database.

The `COMMIT` command is the transactional command used to save changes invoked by a transaction to the database. The `COMMIT` command saves all the transactions to the database since the last `COMMIT` or `ROLLBACK` command.

Following is an example which would delete those records from the table which have age = 25 and then `COMMIT` the changes in the database.

```sql
DELETE FROM CUSTOMERS
   WHERE AGE = 25;
COMMIT;
```
After executing the above statement, you'll get this output:

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  1 | Ramesh   |  32 | Ahmedabad |  2000.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |

#### ROLLBACK 

The `ROLLBACK` command is the transactional command used to undo transactions that have not already been saved to the database. This command can only be used to undo transactions since the last `COMMIT` or `ROLLBACK` command was issued.

Following is an example, which would delete those records from the table which have the age = 25 and then `ROLLBACK` the changes in the database.

```sql
DELETE FROM CUSTOMERS
   WHERE AGE = 25;
ROLLBACK;
```
After executing the above statement, you'll get this output:

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  1 | Ramesh   |  32 | Ahmedabad |  2000.00 |
|  2 | Khilan   |  25 | Delhi     |  1500.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  4 | Chaitali |  25 | Mumbai    |  6500.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |

#### SAVEPOINT 

A `SAVEPOINT` is a point in a transaction when you can roll the transaction back to a certain point without rolling back the entire transaction.

Following is an example where you plan to delete the three different records from the **CUSTOMERS** table. You want to create a SAVEPOINT before each delete, so that you can `ROLLBACK` to any `SAVEPOINT` at any time to return the appropriate data to its original state.

```sql
SAVEPOINT SP1;
```
Savepoint created.

```sql
DELETE FROM CUSTOMERS WHERE ID=1;
```
1 row deleted.

```sql
SAVEPOINT SP2;
```
Savepoint created.

```sql
DELETE FROM CUSTOMERS WHERE ID=2;
```
1 row deleted.

```sql
SAVEPOINT SP3;
```
Savepoint created.

```sql
DELETE FROM CUSTOMERS WHERE ID=3;
```
1 row deleted.

Now that the three deletions have taken place, let us assume that you have changed your mind and decided to ROLLBACK to the SAVEPOINT that you identified as SP2. Because SP2 was created after the first deletion, the last two deletions are undone −

```sql
ROLLBACK TO SP2;
```

Rollback complete.

Notice that only the first deletion took place since you rolled back to SP2.

```sql
SELECT * FROM CUSTOMERS;
```

Output: 

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  2 | Khilan   |  25 | Delhi     |  1500.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  4 | Chaitali |  25 | Mumbai    |  6500.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |

6 rows selected.

#### RELEASE SAVEPOINT 

The `RELEASE SAVEPOINT` command is used to remove a `SAVEPOINT` that you have created.

```sql
RELEASE SAVEPOINT SAVEPOINT_NAME;
```
:::info Note
Once a SAVEPOINT has been released, you can no longer use the ROLLBACK command to undo transactions performed since the last SAVEPOINT.
:::

#### SET TRANSACTION 

The `SET TRANSACTION` command can be used to initiate a database transaction. This command is used to specify characteristics for the transaction that follows. For example, you can specify a transaction to be read only or read write.

```sql
SET TRANSACTION [ READ WRITE | READ ONLY ];
```