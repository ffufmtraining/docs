---
sidebar_position: 19
---

# CASE Statement

The `CASE` statement goes through conditions and returns a value when the first condition is met (like an if-then-else statement). So, once a condition is true, it will stop reading and return the result. If no conditions are true, it returns the value in the `ELSE` clause.

If there is no `ELSE` part and no conditions are true, it returns `NULL`.

## Basic Syntax

```sql
CASE
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
    WHEN conditionN THEN resultN
    ELSE result
END;
```

## DEMO Database

Below is a selection from the "OrderDetails" table in the Northwind sample database:

|OrderDetailID |	OrderID	|ProductID|	Quantity|
|--|---|---|---|
|1|	10248|	11|	12|
|2|	10248|	42|	10|
|3|	10248|	72|	5|
|4|	10249|	14|	9|
|5|	10249|	51|	40|

### Example

The following SQL goes through conditions and returns a value when the first condition is met:

```sql
SELECT OrderID, Quantity,
CASE
    WHEN Quantity > 30 THEN 'The quantity is greater than 30'
    WHEN Quantity = 30 THEN 'The quantity is 30'
    ELSE 'The quantity is under 30'
END AS QuantityText
FROM OrderDetails;
```
After executing the above statement, you'll get this output:


|OrderID|	Quantity|	QuantityText|
|--|---|---|
|10248|	12|	The quantity is under 30|
|10248	|10	|The quantity is under 30|
|10248	|5	|The quantity is under 30|
|10249	|9	|The quantity is under 30|
|10249	|40	|The quantity is greater than 30|

