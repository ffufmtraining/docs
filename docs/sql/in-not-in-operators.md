---
sidebar_position: 9
---

# IN and NOT IN Operators

The `IN` operator allows you to specify multiple values in a `WHERE` clause. It is also a shorthand for multiple `OR` conditions.<br/>
Similarly, you can use the `NOT IN` operator, which is the exact opposite of `IN`

## Basic Syntax

### IN syntax

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name IN (value1, value2, ...);
```
or

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name IN (SELECT STATEMENT);
```
### NOT IN Syntax 

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name NOT IN (value1, value2, ...);
```
or

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name NOT IN (SELECT STATEMENT);
```

## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |

### IN Operator Example

The following SQL statement selects all restaurants that are located in "Makati", or "Taguig":

```sql
SELECT * FROM Restaurants
WHERE City IN ('Makati', 'Taguig');
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |

### NOT IN Operator Example

The following SQL statement selects all restaurants that are NOT located in "Makati", or "Taguig":

```sql
SELECT * FROM Restaurants
WHERE City NOT IN ('Makati', 'Taguig');
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |

