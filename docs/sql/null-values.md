---
sidebar_position: 11
---

# NULL Values

A field with a `NULL` value is a field with no value.

:::info Note
A `NULL` value is different from a zero value or a field that contains spaces. A field with a `NULL` value is one that has been left blank during record creation.
:::

## Basic Syntax

:::caution Note
When testing for `NULL` values, it is not possible to use comparison operators, such as =, <, or < >.
:::

### IS NULL Syntax
```sql
SELECT column_names
FROM table_name
WHERE column_name IS NULL;
```
### IS NOT NULL Syntax
```sql
SELECT column_names
FROM table_name
WHERE column_name IS NOT NULL;
```
## DEMO Database 

Below is an example table named "Restaurants":

| RestaurantID   |     RestaurantName     |                  Address                          | City       |     Country        | 
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|:------------------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   Philippines      |
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   Philippines      |
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   Philippines      |
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   Philippines      |
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   Philippines      |
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  Philippines       |
| 7              |      Kai               |                 null                              |  Makati    |       null         |

### The IS NULL Operator

The `IS NULL` operator is used to test for empty values (NULL values).

The following SQL lists all restaurants with a NULL value in the "Address" field:

```sql
SELECT RestaurantID, RestaurantName, Address, City
FROM Restaurants
WHERE Address IS NULL;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |  
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|
| 7              |      Kai               |                 null                              |  Makati    |     

:::tip 
Always use `IS NULL` to look for `NULL` values.
:::

### The IS NOT NULL Operator

The `IS NOT NULL` operator is used to test for non-empty values (NOT NULL values).

The following SQL lists all restaurants with a value in the "Address" field:

```sql
SELECT RestaurantID, RestaurantName, Address, City
FROM Restaurants
WHERE Address IS NOT NULL;
```
After executing the above statement, you'll get this output:

| RestaurantID   |     RestaurantName     |                  Address                          | City       |  
|----------------|:----------------------:|:-------------------------------------------------:|:----------:|
| 1              |  High Street Cafe      | 30th Street, corner 5th Ave, Shangri-La The Fort  |  Taguig    |   
| 2              |    Blackbird           |   6752 1229 Makati Ave                            |  Makati    |   
| 3              |   Las Flores           |    ADB Ave, Ortigas Center                        |  Pasig     |   
| 4              |   Stockton Place       |    227 Salcedo, Legazpi Village                   |  Makati    |   
| 5              |      Ooma              |    Bonifacio High Street Central, 7th Ave         |  Taguig    |   
| 6              |  Crisostomo            |  2nd Floor, The Newport Mall, Newport Boulevard   | Pasay      |  