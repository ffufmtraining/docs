---
sidebar_position: 18
---

# HAVING Clause

The `HAVING` Clause enables you to specify conditions that filter which group results appear in the results.

The `WHERE` clause places conditions on the selected columns, whereas the `HAVING` clause places conditions on groups created by the `GROUP BY` clause.

## Basic Syntax

The `HAVING` clause must follow the `GROUP BY` clause in a query and must also precede the `ORDER BY` clause if used. The following code block has the syntax of the `SELECT` statement including the `HAVING` clause

```sql
SELECT column1, column2
FROM table1, table2
WHERE [ conditions ]
GROUP BY column1, column2
HAVING [ conditions ]
ORDER BY column1, column2;
```

## DEMO Database

Below is an example table named "Customers":

| ID | NAME     | AGE | ADDRESS   | SALARY   |
|----|----------|-----|-----------|----------|
|  1 | Ramesh   |  32 | Ahmedabad |  2000.00 |
|  2 | Khilan   |  25 | Delhi     |  1500.00 |
|  3 | kaushik  |  23 | Kota      |  2000.00 |
|  4 | Chaitali |  25 | Mumbai    |  6500.00 |
|  5 | Hardik   |  27 | Bhopal    |  8500.00 |
|  6 | Komal    |  22 | MP        |  4500.00 |
|  7 | Muffy    |  24 | Indore    | 10000.00 |


### Example

Below is an example, which would display a record for a similar age count that would be more than or equal to 2.

```sql
SELECT ID, NAME, AGE, ADDRESS, SALARY
FROM CUSTOMERS
GROUP BY age
HAVING COUNT(age) >= 2;
```
After executing the above statement, you'll get this output:

| ID | NAME   | AGE | ADDRESS | SALARY  |
|----|--------|-----|---------|---------|
|  2 | Khilan |  25 | Delhi   | 1500.00 |
