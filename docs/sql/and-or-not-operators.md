---
sidebar_position: 12
---

# AND, OR and NOT Operators

The **AND** and **OR** operators are used to filter records based on more than one condition:

👉 The **AND** operator displays a record if all the conditions separated by **AND** are **TRUE**.<br/>
👉 The **OR** operator displays a record if any of the conditions separated by **OR** is **TRUE**.<br/>

The **NOT** operator displays a record if the condition is **NOT TRUE**.

:::tip
The `WHERE` clause can be combined with `AND`, `OR`, and `NOT` operators.
:::

## Basic Syntax

### AND
```sql
SELECT column1_name, column2_name, column3_name
FROM table_name
WHERE condition1 AND condition2;
```

### OR
```sql
SELECT column1_name, column2_name, column3_name
FROM table_name
WHERE condition1 OR condition2 OR condition3;
```

### NOT
```sql
SELECT column1_name, column2_name, column3_name
FROM table_name
WHERE NOT condition;
```
## DEMO Database 

Below is an example table named "Students":

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010225         | Ben White              |  London                  |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |       
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |
| 010230         | Debbie Fischer         |  Cambridge               | England              |

### AND Example

The following SQL statement selects all fields from "Students" where country is "Germany" AND city is "Frankfurt":

```sql
SELECT * FROM Students
WHERE Country='Germany' AND City='Frankfurt';
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010223         | Hans Weber             | Frankfurt                | Germany              |
| 010228         |  Jürgen Koch           |  Frankfurt               | Germany              |
| 010229         |  Jürgen Wagner         |  Frankfurt               |  Germany             |

### OR Example

The following SQL statement selects all fields from "Students" where city is "Rome" OR "Riga":

```sql
SELECT * FROM Students
WHERE City='Rome' OR City='Riga';
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |  

### NOT Example

The following SQL statement selects all fields from "Students" where country is NOT "Germany":

```sql
SELECT * FROM Students
WHERE NOT Country='Germany';
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010222         | Mario Rossi            | Rome                     | Italy                |
| 010224         | Lucia Perrotta         | Rome                     |  Italy               |
| 010225         | Ben White              |  London                  |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              |  
| 010230         | Debbie Fischer         |  Cambridge               | England              |

### Combining AND, OR and NOT

You can also combine the `AND`, `OR` and `NOT` operators.

The following SQL statement selects all fields from "Students" where country is "England" AND city must be "Cambridge" OR "London":

:::tip 
Use parenthesis to form complex expressions.
:::

```sql
SELECT * FROM Students
WHERE Country='England' AND (City='Cambridge' OR City='London');
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010225         | Ben White              |  London                  |  England             |
| 010230         | Debbie Fischer         |  Cambridge               | England              |

The following SQL statement selects all fields from "Students" where country is NOT "Germany" and NOT "Italy":

```sql
SELECT * FROM Students
WHERE NOT Country='Germany' AND NOT Country='Italy';
```
After executing the above statement, you'll get this output:

|    StudentID   |     StudentName        |       City               |        Country       |
|----------------|:----------------------:|:------------------------:|:--------------------:|
| 010225         | Ben White              |  London                  |  England             |
| 010226         | Pierre Pavard          |   Paris                  |  France              |
| 010227         |  Juris Tal             |  Riga                    |  Latvia              | 
| 010230         | Debbie Fischer         |  Cambridge               | England              |
