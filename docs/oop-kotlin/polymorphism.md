---
sidebar_position: 7
---

# Polymorphism
When any variable, function, or object has more than one form, this concept is known as polymorphism. The two types of polymorphism are **static** and **dynamic**.

## Static Polymorphism
It is also known as compile time polymorphism. When methods have the same name but different signature, which are used to performing different kinds of operations.

```kotlin
class MethodOverloading {  
  fun area(a:Int):Int {  
    return a*a  
  }  

  fun area(length:Int,height:Int):Int {  
    return length*height  
  }  

  fun area(base:Float,height:Float):Float {  
    return (base*height)/2  
  }  
}  

fun main(args:Array<String>) {  
  var obj=MethodOverloading()  
  println("Area of Square="+obj.area(5))  
  println("Area of Rectangle="+obj.area(5,4))  
  println("Area of Triangle="+obj.area(10.05f,5.5f))  
}  
 
// Area of Square=25 
// Area of Rectangle=20 
// Area of Triangle=27.6375 
```

All these three methods are having the same name but they are used to calculate the areas of a square, a rectangle, and a triangle respectively.

## Dynamic Polymorphism
It is also known as runtime polymorphism or late binding. It is used to decide which block of code will be executed, after the function call at runtime.

```kotlin
open class Car(var speed:Int) {  
  open  fun show() {  
    println("car is running on $speed km/hrs")  
  }  
}  

class Mercedize(var mspeed:Int) : Car(mspeed) {  
  override fun show() {  
    println("Mercedize is running on $mspeed km/hrs")  
  }  
}  

fun main(args:Array<String>) {  
  var newcar=Mercedize(150);  
  println(newcar.show())  
}  

// Mercedize is running on 150 km/hrs 
```

## Additional Learning Materials
🔗 [C# Corner - OOP Implementation in Kotlin](https://www.c-sharpcorner.com/article/oop-implementation-in-kotlin/)