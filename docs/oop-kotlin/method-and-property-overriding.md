---
sidebar_position: 6
---

# Method and Property Overriding
You can override the properties of a super class as well. To allow child classes to override a property of a parent class, you must annotate it with the `open` modifier. Moreover, The child class must use `override` keyword for overriding a property of a parent class.

```kotlin
open class Employee {
  // Use "open" modifier to allow child classes to override this property
  open val baseSalary: Double = 30000.0
}

class Programmer : Employee() {
  // Use "override" modifier to override the property of base class
  override val baseSalary: Double = 50000.0
}

fun main(args: Array<String>) {
  val employee = Employee()
  println(employee.baseSalary) // 30000.0

  val programmer = Programmer()
  println(programmer.baseSalary) // 50000.0
}
```

## Using a getter/setter
You can override a super class property either using an initializer or using a custom getter/setter.

```kotlin
open class Person {
  open var age: Int = 1
}

class CheckedPerson: Person() {
  override var age: Int = 1
  set(value) {
    field = if(value > 0) value else throw IllegalArgumentException("Age can not be negative")
  }
}

fun main(args: Array<String>) {
  val person = Person()
  person.age = -5 // Works

  val checkedPerson = CheckedPerson()
  checkedPerson.age = -5  // Throws IllegalArgumentException : Age can not be negative
}
```

When you override a property or a member function of a super class, the super class implementation is shadowed by the child class implementation. You can access the properties and functions of the super class using `super()` keyword.

```kotlin
open class Employee {
  open val baseSalary: Double = 10000.0

  open fun displayDetails() {
    println("I am an Employee")
  }
}

class Developer: Employee() {
  override var baseSalary: Double = super.baseSalary + 10000.0

  override fun displayDetails() {
    super.displayDetails()
    println("I am a Developer")
  }
}
```

## Additional Learning Materials
🔗 [Callicoder](https://www.callicoder.com/kotlin-inheritance/) 