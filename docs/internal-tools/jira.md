---
sidebar_position: 2
---

# JIRA 📋

Jira is a suite of agile work management solutions that powers collaboration across all teams from concept to customer, empowering you to do the best work of your life, together. Jira offers several products and deployment options that are purpose-built for Software, IT, Business, Ops teams, and more. Read on to see which is right for you.

## About JIRA 

Jira helps teams plan, assign, track, report, and manage work and brings teams together for everything from agile software development and customer support to start-ups and enterprises. 

Software teams build better with Jira Software, the #1 tool for agile teams. Deliver amazing service experiences across all teams from IT, Dev, Ops, and more with Jira Service Management. Business teams can unlock the power of agile and collaborate better with Jira Work Management. Jira Align is an enterprise agile planning platform that connects work at scale.

## Key Terms

### Issues

A Jira 'issue' refers to a single work item of any type or size that is tracked from creation to completion. For example, an issue could be a feature being developed by a software team, a to-do item for a marketing team, or a contract that needs to be written by a legal team.

:::tip 
Other commonly used terms for issues are 'requests', 'tickets' or 'tasks'. We recommend using 'issues' to help your team stay on the same page when working across the Jira product family. 
:::

### Projects

A project is, quite simply, a collection of issues that are held in common by purpose or context. Issues grouped into projects can be configured in a variety of ways, ranging from visibility restrictions to available workflows.

### Boards

A board in Jira software is a part of a project that displays issues giving teams a flexible way to view, manage, and reporting on work in progress. Simply put, a board is a visual representation of a team’s workflow within a project.

### Workflows

Workflows represent the sequential path an issue takes from creation to completion. A basic workflow might look something like this:

![Workflows](/img/workflow.svg "Workflows")

In this case, Open, Done, and the labels in between represent the status an issue can take, while the arrows represent potential transitions from one status to another. Workflows can be simple or complex, with conditions, triggers, validators, and post functions. We'll dive deeper into these advanced configurations later in this guide. For now, it is recommended for novice Jira Software admins to keep their workflows as simple as possible, until business needs to determine the requirements for complex workflow configurations. 

### Agile 

Agile is not a Jira Software-specific term. It's a work philosophy that originated in the software development field and has since expanded to a variety of other industries. While we won't belabor the definition here (there are great agile resources for that!), agile emphasizes an iterative approach to work informed by customer feedback where delivery occurs incrementally and continuously. The ideal agile team can move quickly and adapt to changing requirements without missing much of a beat.

## Getting Started

<iframe width="950" height="600" src="https://www.youtube.com/embed/iTdnMWa6Jdw?list=PLaD4FvsFdarR9RNlvUfee_iJ6WKRsRJn4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Additional Learning Materials
🔗 [JIRA](https://www.atlassian.com/software/jira/guides/getting-started/basics#step-1-create-a-project)