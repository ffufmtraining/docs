---
sidebar_position: 1
---

# Getting Started
This **Spring Boot Bootcamp** will provide quality learning experiences that will allow you, learners, to develop and improve your skills in Kotlin, SQL, Spring Boot, Databases, Project Management and more which are needed to make it in the very competitive job market. 

Our online instructor-led training sessions will also provide tools that will help you understand difficult information easily while engaging with others in creating and discovering your own knowledge.

## Objectives

The goal is to be able to build fully functional, robust and efficient applications using Kotlin, SQL, and Spring Boot by learning how to:

- Write RESTful services in Kotlin via Spring Boot
- Write unit tests for Spring including using automatic mock injection
- Use Spring Boot starters for data access and security
- Leverage Kotlin to simplify building Spring Boot applications


## Prerequisites

A basic understanding in Java, Kotlin, SQL, and Spring Boot is a plus. However, if you consider yourself a beginner with no coding experience, you can still take and get through this course as long as you have the passion, grit, and determination. Our team will help you deal with learning blockers and assist you throughout the duration of the course. 👌 

## Class Format

### Synchronous Learning

Online instructor-led lectures via peer-to-peer video communications service platform

### Asynchronous Learning

Throughout the duration of the course, you will need to complete four(4) mini projects and a final capstone project individually based on the basic concepts, methods and principles taught in class. The purpose of this experience is to allow you to apply what you have learned to real world projects and ultimately prepare you for FFUF duties once you finish the course. 


## Technical Requirements

| | |
|-------------------------------|----|
| Operating System |   Windows 10 (64-bit) (excluding ‘S Mode’) <br/> Windows 8.1 (64-bit) (all editions except ‘RT’) <br/> Mac OS 10.13 and above (excluding beta versions) |
| RAM | OS specified minimum RAM 4 GB RAM or more |
| Display | Minimum Resolution: 1024 x 768 in 16-bit color. <br/> Additional monitors are encouraged|
| Internet Browser | The newest versions of Microsoft Edge, Safari, Chrome, and Firefox |
| Internet Connection | For optimal performance, a reliable and stable connection speed of 3 Mbps down/up is required |
| Webcam | Webcam must have a minimum resolution of 640×480 @ 10 fps|
| Sound & Microphone | Verify the audio and microphone are not set on mute |
| Power | Make sure you are connected to a stable power source before starting your each session to avoid draining your battery during class |
| Device | Tablets are not forbidden especially if it will be used as a secondary device, however, your main device must have a physical keyboard, monitor, and also meets the operating system requirements mentioned earlier|





