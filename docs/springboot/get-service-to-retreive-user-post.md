---
sidebar_position: 19
---

# Implementing a GET service to retrieve all Posts of a User

We will retrieve all the posts of a specific user.

**Step 1:** Open the UserJPAResource.java file and create a mapping for the URI "/jpa/users/{id}/posts"

```java
@GetMapping("/jpa/users/{id}/posts")  
public List<Post> retriveAllUsers(@PathVariable int id)  
{  
  Optional<User> userOptional= userRepository.findById(id);  
  if(!userOptional.isPresent())  
  {  
    throw new UserNotFoundException("id: "+ id);  
  }  
  return userOptional.get().getPosts();  
}  
```

**Step 2:** There is no need to show user detail in the response, so we will add @JsonIgnore annotation just above the User field in Post.java file.

**Step 3:** Open the Postman and send a GET request with the URI http://localhost:8080/jpa/users/{id}/posts. In our case, we have specified user id 101. It shows all the posts done by user 101.

```java
[  
  {  
    "id": 111,  
    "description": "first post"  
  },  
  {  
    "id": 112,  
    "description": "second post"  
  }  
]  
```

Now, we send a GET request for the user who has not created any post yet. The user 105 has not created any post so we will specify this user id in the URI http://localhost:8080/jpa/users/105/posts.

It shows a pair of empty square brackets. The brackets denote that the user exists, but the user has not created any post.

Again send a GET request for the user who does not exist in the database say 110. It shows the Status: 404 Not Found with the following details:

```java
{  
"timesatmp": "2019-10-05T05:31:09.407+0000",  
"message": "id-110",  
"details": "uri=/jpa/users/110/posts"  
}  
```

