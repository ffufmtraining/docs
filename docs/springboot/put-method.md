---
sidebar_position: 22
---

# PUT method

## PUT vs POST

In a typical REST architecture, a client sends requests in the form of HTTP methods to the server to create, retrieve, modify, or destroy resources. While both PUT and POST can be used to create resources, there are significant differences between them in terms of their intended applications.

According to the RFC 2616 standard, the POST method should be used to request the server to accept the enclosed entity as a subordinate of the existing resource identified by the Request-URI. This means the POST method call will create a child resource under a collection of resources.

On the other hand, the PUT method should be used to request the server to store the enclosed entity under the provided Request-URI. If the Request-URI points to an existing resource on the server, the supplied entity will be considered a modified version of the existing resource. Therefore, the PUT method call will either create a new resource or update an existing one.

Another important difference between the methods is that PUT is an idempotent method while POST is not. For instance, calling the PUT method multiple times will either create or update the same resource. On the contrary, multiple POST requests will lead to the creation of the same resource multiple times.

### Sample Application

To demonstrate the difference between PUT and POST, we're going to create a simple RESTful web application using Spring Boot. The application will store the names and addresses of people.

#### Maven Dependencies

To begin with, we need to include the dependencies for Spring Web, Spring Data JPA, and the in-memory H2 database in our pom.xml file:

```java
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
  <groupId>com.h2database</groupId>
  <artifactId>h2</artifactId>
  <scope>runtime</scope>
</dependency>
```

#### Domain Entity and Repository Interface

Let's start by creating the domain object first. For the address book, let's define an Entity class called Address that we'll use to store the address information of individuals. For the sake of simplicity, we're going to use three fields – name, city, and postalCode – for our Address entity:

```java
@Entity
public class Address {

  private @Id @GeneratedValue Long id;
  private String name;
  private String city;
  private String postalCode;

  // constructors, getters, and setters

}
```

The next step is to access the data from the database. For simplicity, we'll leverage Spring Data JPA's JpaRepository. This will allow us to perform CRUD functionalities on the data without writing any additional code:

```java
public interface AddressRepository extends JpaRepository<Address, Long> {
}
```

#### REST Controller

Finally, we need to define the API endpoints for our application. We'll create a RestController that will consume HTTP requests from the client and send back the appropriate response.

Here, we'll define a @PostMapping for creating new addresses and storing them in the database and a @PutMapping to update the content of the address book based on the request URI. If the URI is not found, it will create a new address and store it in the database:

```java
@RestController
public class AddressController {

  private final AddressRepository repository;

  AddressController(AddressRepository repository) {
    this.repository = repository;
  }

  @PostMapping("/addresses")
  Address createNewAddress(@RequestBody Address newAddress) {
    return repository.save(newAddress);
  }

  @PutMapping("/addresses/{id}")
  Address replaceEmployee(@RequestBody Address newAddress, @PathVariable Long id) {

    return repository.findById(id)
        .map(address -> {
          address.setCity(newAddress.getCity());
          address.setPin(newAddress.getPostalCode());
          return repository.save(address);
        })
        .orElseGet(() -> {
          return repository.save(newAddress);
        });
  }
  //additional methods omitted
}
```

#### cURL Requests

Now we can test our developed application by using cURL to send sample HTTP requests to our server.

For creating a new address, we'll enclose the data in JSON format and send it through a POST request:

```java
curl -X POST --header 'Content-Type: application/json' \
    -d '{ "name": "John Doe", "city": "Berlin", "postalCode": "10585" }' \ 
    http://localhost:8080/addresses
```

Now, let's update the content of the address we created. We'll send a PUT request using the id of that address in the URL. In this example, we will update the city and the postalCode section of the address we just created — we'll suppose it was saved with id=1:

```java
curl -X PUT --header 'Content-Type: application/json' \
  -d '{ "name": "John Doe", "city": "Frankfurt", "postalCode": "60306" }' \ 
  http://localhost:8080/addresses/1
```

## PUT vs PATCH

**When to Use Put and When Patch?**

Let's start with both a simple and a slightly simple statement.

***When a client needs to replace an existing Resource entirely, they can use PUT. When they're doing a partial update, they can use HTTP PATCH.***

For instance, when updating a single field of the Resource, sending the complete Resource representation can be cumbersome and uses a lot of unnecessary bandwidth. In such cases, the semantics of PATCH make a lot more sense.

Another important aspect to consider here is idempotence. PUT is idempotent; PATCH can be idempotent but isn't required to be. So, depending on the semantics of the operation we're implementing, we can also choose one or the other based on this characteristic.

### Implementing PUT and PATCH Logic

Let's say we want to implement the REST API for updating a HeavyResource with multiple fields:

```java
public class HeavyResource {
  private Integer id;
  private String name;
  private String address;
  // ...
}
```

First, we need to create the endpoint that handles a full update of the resource using PUT:

```java
@PutMapping("/heavyresource/{id}")
public ResponseEntity<?> saveResource(@RequestBody HeavyResource heavyResource,
  @PathVariable("id") String id) {
    heavyResourceRepository.save(heavyResource, id);
    return ResponseEntity.ok("resource saved");
}
```

This is a standard endpoint for updating resources.

Now let's say that address field will often be updated by the client. In that case, we don't want to send the whole HeavyResource object with all fields, but we do want the ability to only update the address field — via the PATCH method.

We can create a HeavyResourceAddressOnly DTO to represent a partial update of the address field:

```java
public class HeavyResourceAddressOnly {
  private Integer id;
  private String address;
    
  // ...
}
```

Next, we can leverage the PATCH method to send a partial update:

```java
@PatchMapping("/heavyresource/{id}")
public ResponseEntity<?> partialUpdateName(
  @RequestBody HeavyResourceAddressOnly partialUpdate, @PathVariable("id") String id) {
    
    heavyResourceRepository.save(partialUpdate, id);
    return ResponseEntity.ok("resource address updated");
}
```

With this more granular DTO, we can send the field we need to update only, without the overhead of sending the whole HeavyResource.

If we have a large number of these partial update operations, we can also skip the creation of a custom DTO for each out — and only use a map:

```java
@RequestMapping(value = "/heavyresource/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity<?> partialUpdateGeneric(
  @RequestBody Map<String, Object> updates,
  @PathVariable("id") String id) {
    
    heavyResourceRepository.save(updates, id);
    return ResponseEntity.ok("resource updated");
}
```

This solution will give us more flexibility in implementing API, but we do lose a few things as well, such as validation.

### Testing PUT and PATCH

Finally, let's write tests for both HTTP methods.

First, we want to test the update of the full resource via PUT method:

```java
mockMvc.perform(put("/heavyresource/1")
  .contentType(MediaType.APPLICATION_JSON_VALUE)
  .content(objectMapper.writeValueAsString(
    new HeavyResource(1, "Tom", "Jackson", 12, "heaven street")))
  ).andExpect(status().isOk());
```

Execution of a partial update is achieved by using the PATCH method:


```java
mockMvc.perform(patch("/heavyrecource/1")
  .contentType(MediaType.APPLICATION_JSON_VALUE)
  .content(objectMapper.writeValueAsString(
    new HeavyResourceAddressOnly(1, "5th avenue")))
  ).andExpect(status().isOk());
```

We can also write a test for a more generic approach:

```java
HashMap<String, Object> updates = new HashMap<>();
updates.put("address", "5th avenue");

mockMvc.perform(patch("/heavyresource/1")
    .contentType(MediaType.APPLICATION_JSON_VALUE)
    .content(objectMapper.writeValueAsString(updates))
  ).andExpect(status().isOk());
```

### Handling Partial Requests With Null Values

When we are writing an implementation for a PATCH method, we need to specify a contract of how to treat cases when we get null as a value for the address field in the HeavyResourceAddressOnly.

Suppose that client sends the following request:

```java
{
  "id" : 1,
  "address" : null
}
```

Then we can handle this as setting a value of the address field to null or just ignoring such a request by treating it as no-change.

We should pick one strategy for handling null and stick to it in every PATCH method implementation.

