---
sidebar_position: 18
---

# Updating GET methods on User Resource

Using the UserResource which talks to the in-memory, we will create a new UserResource that will talk to the embedded database. 

**Step 1:** Copy the UserResource.java file and paste it in the user package. Rename it with UserJPAResource.

**Step 2:** Now, we have two URIs with the same name that create conflict. To remove this conflict, we will add /jpa in UserJPAResource.java file.

### UserJPAResource.java

```java
package com.javatpoint.server.main.user;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.net.URI;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserJPAResource {
  @Autowired
  private UserDaoService service;

  @GetMapping("/jpa/users")
  public List<User> retriveAllUsers() {
    return service.findAll();
  }

  @GetMapping("/jpa/users/{id}")
  public Resource<User> retriveUser(@PathVariable int id) {
    User user = service.findOne(id);
    if (user == null)
    //runtime exception  
      throw new UserNotFoundException("id: " + id);
    //"all-users", SERVER_PATH + "/users"  
    //retrieveAllUsers  
    Resource<User> resource = new Resource<User>(user);   //constructor of Resource class  
    //add link to retrieve all the users  
    ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retriveAllUsers());
    resource.add(linkTo.withRel("all-users"));
    return resource;
  }

  //method that delete a user resource  
  @DeleteMapping("/jpa/users/{id}")
  public void deleteUser(@PathVariable int id) {
    User user = service.deleteById(id);
    if (user == null)
      //runtime exception  
      throw new UserNotFoundException("id: " + id);
  }

  //method that posts a new user detail and returns the status of the user resource  
  @PostMapping("/jpa/users")
  public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
    User sevedUser = service.save(user);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(sevedUser.getId()).toUri();
    return ResponseEntity.created(location).build();
  }
}  
```

But it is not really talking to the database. We need to create a spring data repository.

**Step 3:** Create an interface with the name UserRepository that extends JpaRepository. Specify the entity that has to be managed. We have specified User and Integer. Now we have the UserRepository ready.

### UserRepository.java

```java
package com.javatpoint.server.main.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}  
```

**Step 4:** Create the use of UserResource. We have autowired the UserRepository interface in the UserJPAResource class.

```
@Autowired  
private UserRepository userRepository;  
```

**Step 5:** Return the userRepository.findAll() in the retriveAllUsers() method.

```java
@GetMapping("/jpa/users")  
public List<User> retriveAllUsers()  
{  
  return userRepository.findAll();  
}  
```

The retriveAllUsers() is the only method that retrieve data form the embedded database, all the other methods retrieve data from the static array list.

**Step 6:** Open the Postman. Type the URI http://localhost:8080/jpa/users and send a GET request. It shows all the data that is fetched from the embedded database.

![JPA](/img/postman15.png "GET request")

Again send a GET request with the URL http://localhost:8080/jpa/users/1. It returns the specified user id, i.e. 1, but it picks up data from memory.

But we are required to fetch data from the embedded database. We need to change the following services in the UserJPAResource.java.

```java
@GetMapping("/jpa/users")  
public List<User> retriveAllUsers()  
{  
  return userRepository.findAll();  
}  
```
In the following service, the findById() returns the Option of User whether user is null or not null. Whenever we use findById(), there is two possibilities: id exist or not exist. When it does not exist it comes with a proper object. We will check the user exist or not by the statement if(!user.isPresent()). If the user is not present it throws an exception.

```java
@GetMapping("/jpa/users/{id}")  
public Resource<User> retriveUser(@PathVariable int id)  
{  
  Optional<User> user= userRepository.findById(id);  
  if(user.isPresent())  
  //runtime exception  
  throw new UserNotFoundException("id: "+ id);  
  //"all-users", SERVER_PATH + "/users"  
  //retrieveAllUsers  
  Resource<User> resource=new Resource<User>(user.get()); //constructor of Resource class  
  //add link to retrieve all the users  
  ControllerLinkBuilder linkTo=linkTo(methodOn(this.getClass()).retriveAllUsers());  
  resource.add(linkTo.withRel("all-users"));  
  return resource;  
}  
```

Again send a GET request with the URL http://localhost:8080/jpa/users/1. It returns a specified user and the link to /jpa/users.

```java
{  
  "name": "John",  
  "dob": "2019-10-01"T0726:52.596+0000",  
  "_links": {  
  "all-users": {  
  "href": "http://localhost:8080/jpa/users"  
}  
```

