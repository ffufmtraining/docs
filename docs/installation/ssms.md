---
sidebar_position: 5
---

# SQL Server Management Studio 💽

SQL Server Management Studio (SSMS) is an integrated environment for managing any SQL infrastructure. Use SSMS to access, configure, manage, administer, and develop all components of SQL Server, Azure SQL Database, and Azure Synapse Analytics.

## System requirements

The current release of SSMS supports the following 64-bit platforms when used with the latest available service pack:

### Supported Operating Systems:

- Windows Server 2022 (64-bit)
- Windows 11 (64-bit)
- Windows 10 (64-bit) version 1607 (10.0.14393) or later
- Windows 8.1 (64-bit)
- Windows Server 2019 (64-bit)
- Windows Server 2016 (64-bit)
- Windows Server 2012 R2 (64-bit)
- Windows Server 2012 (64-bit)
- Windows Server 2008 R2 (64-bit)

### Supported hardware:

- 1.8 GHz or faster x86 (Intel, AMD) processor. Dual-core or better recommended
- 2 GB of RAM; 4 GB of RAM recommended (2.5 GB minimum if running on a virtual machine)
- Hard disk space: Minimum of 2 GB up to 10 GB of available space

:::info Note
SSMS is available only as a 32-bit application for Windows. If you need a tool that runs on operating systems other than Windows, we recommend Azure Data Studio. Azure Data Studio is a cross-platform tool that runs on macOS, Linux, as well as Windows. For details, see Azure Data Studio.
:::

## Download SQL Server Management Studio
🔗 [Download SQL Server Management Studio (SSMS) 18.10](https://aka.ms/ssmsfullsetup)

