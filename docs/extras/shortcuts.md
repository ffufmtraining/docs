---
sidebar_position: 1.1
---

# Shortcuts
IntelliJ IDEA has keyboard shortcuts for most of its commands related to **editing**, **navigation**, **refactoring**, **debugging**, and pretty much everything else we might want to do regularly. Learning and practicing these shortcuts so they get loaded into our muscle memory can help us to stay in the state of flow.

## Alt + Enter
It is the most well known. This keyboard shortcut can be used to fix just about anything by showing us actions for the current context. When we see an error in our code, we can place the cursor on the error, and press **Alt+Enter** we get a list of suggested fixes for the problem.

## F2
We don’t want to use the mouse when navigating between errors and warnings in the editor, so use **F2** to jump to the next error, warning or suggestion. Combine this with either **Alt+Enter**, to see all suggestions and pick one, or **Shift+Alt+Enter** to apply the first suggestion.

## Alt+1 
We don’t need the mouse to open tool windows either. **⌘1** (MacOS) or **Alt+1** (Windows/Linux) opens the project window and puts the focus in there. You can navigate the tree using the arrow keys and search by typing.

## Esc
To put the focus back on the editor, press **escape**.  Whatever tool window is open, this will put you back in the editor so you can carrying on working with the code.  In fact, escape is useful for closing any popup without applying changes.

## Ctrl+E
The project window may not be the best way to navigate to the file we want. We can view the most recently opened files using **⌘E** (MacOS) or **Ctrl+E** (Windows/Linux).  This pops up the recent files box which you can navigate using arrow keys. We can also open tool windows from here, including ones that don’t have keyboard shortcuts.  Like any window in IntelliJ IDEA we can search for something specific in here by typing.

## Ctrl+B
We often want to navigate the code from within the code. With **⌘B** (MacOS), or **Ctrl+B** (Windows/Linux) we can go to the declaration of a symbol. Pressing this on a field will take the cursor to the field declaration.  Pressing it on a class name will take us to the class file.  If we press **⌥⌘B** (MacOS) or **Ctrl+Alt+B** (Windows/Linux), we can navigate to an implementation instead.

## Alt+F7
Instead of finding the declaration, often we want to find where something is used.  Alt+F7 will show us all the places something is used.  If we press **Alt+F7** on an interface name, the search window will show all the places the interface is used, whether it’s a field declaration or a class that implements this interface.

## Ctrl Ctrl
We can run anything from anywhere by pressing the **Ctrl** key twice.  No matter where we are in the IDE or which file is open, if we double tap Ctrl the **Run Anything** window opens.  By default, this shows a list of recently run configurations.  But we can also type the name of something to run to search for other run configurations.

## Shift shift
The ultimate shortcut is *search everywhere*.  Double pressing the **shift** key opens a search box that lets us look for anything. Like find action, we can use it for changing settings. By default, the search box shows the recent files, so we can use this instead of ⌘E / Ctrl+E.  If we type something to look for, we can see results from classes, files, symbols, and actions.  Search everywhere also supports commands, so for example we can search for settings for the editor.

## Ctrl+W
We can select increasing or decreasing sections of code near the cursor with **⌥** and Up or Down arrows (MacOS) and **Ctrl+W** or **Ctrl+Shift+W** (Windows/Linux).  When extending the selection, IntelliJ IDEA automatically selects the next valid expression in increasing sections.  **⌥↓** (MacOS) or **Ctrl+Shift+W** (Windows/Linux), will decrease the selection again all the way back to the cursor.

## Ctrl + /
Pressing **⌘/** (MacOS) or **Ctrl+/** (Windows/Linux) anywhere on a line will comment out this line of code with a line comment.  Pressing the same shortcut will un-comment the line if it is already commented.

If we select a whole code block, we can use **⌥⌘/** (MacOS) or **Shift+Ctrl+/** (Windows/Linux) to add a block *comment*. Pressing this shortcut again with the cursor anywhere inside the code block will remove the block comment.

## Shift+Ctrl+Enter
If we’re in the habit of using complete current statement while you’re coding, most of the time it will simply add a semi-colon to the end of the code. But it works for more complex code. If you press it while you’re writing a `for` loop, IntelliJ IDEA will add the curly braces and place your cursor inside the block.  In an `if` statement, it can add the parentheses, curly braces and place your cursor in the correct spot. Even if the IDE doesn’t need to add any more code to finish your statement, it’s useful to use this shortcut to put the cursor where you next need it.

## Ctrl+Alt+L
We can easily format the current file to the project’s standards using **⌥⌘L** (MacOS), or **Ctrl+Alt+L** (Windows/Linux).  This can be set to either just format the lines that have changed in the file, or the whole file. Formatting can even add curly braces if this is required by our standards. If we want to specify a different scope to format we can press **⇧⌥⌘L** (MacOS) or **Shift+Ctrl+Alt+L** (Windows/Linux), and select to reformat the whole file.

## Shift+Ctrl+Alt+T
Most of the automated refactorings in IntelliJ IDEA have their own shortcuts, but we can access all of them with one shortcut-- **⌃T** (MacOS) or **Shift+Ctrl+Alt+T** (Windows/Linux).  When we press this shortcut on a symbol or selection we are shown the *refactoring* options available.  We can select one with the arrow keys and enter, or we can use the number to the left of the refactoring to select it. The dialog also shows us the keyboard shortcut for this specific refactoring if it exists so we can use it directly next time.

## Shift+Ctrl+A
We don’t have to remember all of these shortcuts. Use *Find Action*, **⇧⌘A** (MacOS) or **Shift+Ctrl+A** (Windows/Linux), to search for any action in IntelliJ IDEA. The drop down will show not only the actions, but also the shortcut so that we can learn this shortcut and practice it.  Find action will let us search for actions of course, but also settings so we can change settings directly from here.  We can also search for and open tool windows.

