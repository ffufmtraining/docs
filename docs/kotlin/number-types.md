---
sidebar_position: 4
---

# Number Types

For integer numbers, there are four types with different sizes, hence, value ranges.

## Integer types
 
| Type           |    Size (bits)      |             Min value                   |           Max value                  | 
|----------------|:-------------------:|:---------------------------------------:|:------------------------------------:|
| Byte           |          8          |              -128                       |              127                     |   
| Short          |          16         |             -32768                      |              32767                   | 
| Int            |          32         |    -2,147,483,648 (-2 31)               |      2,147,483,647 (2 31- 1)         |   
| Long           |          64         |    -9,223,372,036,854,775,808 (-2 63)   |  9,223,372,036,854,775,807 (2 63- 1) |   

To specify the `Long` value explicitly, append the suffix `L` to the value.

```kotlin
val one = 1 // Int
val threeBillion = 3000000000 // Long
val oneLong = 1L // Long
val oneByte: Byte = 1
```

## Floating Types
According to the IEEE 754 standard, floating point types differ by their decimal place. `Float` reflects the IEEE 754 **single precision**, while `Double` provides **double precision**.


| Type           |      Size (bits)     |       Significant Bits      |        Exponent Bits       |     Decimal Digits     |
|----------------|:--------------------:|:---------------------------:|:--------------------------:|:----------------------:|
| Float          |         32           |             24              |            8               |            6-7         |
| Double         |         64           |             53              |            11              |            15-16       |

For variables initialized with fractional numbers, the compiler infers the `Double` type.

```kotlin
// Double
val pi = 3.14 

// Double
val oneDouble = 1.0 

// Double
val one: Double = 1.0 

// error: type mismatch
val one: Double = 1 
```
To explicitly specify the `Float` type for a value, add the suffix `f` or `F`. If such a value contains more than 6-7 decimal digits, it will be rounded.

```kotlin
val e = 2.7182818284 // Double
val eFloat = 2.7182818284f // Float, actual value is 2.7182817
```

There are no implicit widening conversions for numbers in Kotlin. For example, a function with a `Double` parameter can be called only on `Double` values.

```kotlin
fun main() {
  fun printDouble(d: Double) { print(d) }

  val i = 1
  val d = 1.0
  val f = 1.0f

  printDouble(d) // 1.0
  printDouble(i) // error: type mismatch
  printDouble(f) // error: type mismatch
}
```
:::note
To convert numeric values to different types, use explicit conversions.
:::

## Additional Learning Materials
🔗 [Kotlin Documentation](https://kotlinlang.org/docs/basic-types.html)