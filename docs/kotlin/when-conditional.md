---
sidebar_position: 16
---

# When Conditional
It is used to handle the situation in a nicer way. Using **when** expression is far easy and more clean, in comparison to writing many `if else` expressions. 

:::tip
It is similar to the **switch statement** in C, C++ and Java.
:::

It matches its argument against all branches sequentially until some branch condition is satisfied.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  val day = 2

  // executes the code where the branch condition is satisfied
  val result = when (day) {
    1 -> "Monday"
    2 -> "Tuesday"
    3 -> "Wednesday"
    4 -> "Thursday"
    5 -> "Friday"
    6 -> "Saturday"
    7 -> "Sunday"
    else -> "Invalid day."
  }

  println(result)
  // Tuesday
}

// Tuesday
```
It can be used either as an expression or as a statement, simply like a switch statement in Java. If it is used as an expression, the value of the first matching branch becomes the value of the overall expression.

It can also be written this way.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  val day = 2
  
  // executes the code where the branch condition is satisfied
  when (day) {
    1 -> println("Monday")
    2 -> println("Tuesday") // condition is satisfied
    3 -> println("Wednesday")
    4 -> println("Thursday")
    5 -> println("Friday")
    6 -> println("Saturday")
    7 -> println("Sunday")
    else -> println("Invalid day.")
  }
}

// Tuesday
```

## Combining When Conditions
We can combine multiple when conditions into a **single condition**.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  val day = 2

  // prints "Weekday" if the argument is 1, 2, 3, 4, 5 and "Weekend" if not
  when (day) {
    1, 2, 3, 4, 5 -> println("Weekday")
    else -> println("Weekend")
  }
}

// Weekday
```

## Range in When Conditions
They are created using double dots `..` and we can use them while checking when condition with the help of `in` operator.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  val day = 2

  // prints "Weekday" if the argument is 1 and 5 and "Weekend" if not
  when (day) {
    in 1..5 -> println("Weekday")
    else -> println("Weekend")
  }
}

// Weekday
```

## Expression in When Conditions
It can use arbitrary expressions instead of a constant as branch condition.

```kotlin
fun main(args: Array<String>) {
  // declares Int variables
  val x = 20
  val y = 10
  val z = 10
   
  // checks if the value of x is equal to the sum of y and z
  when (x) {
    (y+z) -> print("y + z = x = $x")
    else -> print("Condition is not satisfied")
  }
}

// y + z = x = 20
```

## With Block of Code
It can be put as block of code enclosed within **curly braces**.

```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  val day = 2

  // executes the code where the branch condition is satisfied
  when (day) {
    1 -> {
      println("First day of the week")
      println("Monday")
    }
    2 -> {
      println("Second day of the week")
      println("Tuesday")
    }
    3 -> {
      println("Third day of the week")
      println("Wednesday")
    }
    4 -> println("Thursday")
    5 -> println("Friday")
    6 -> println("Saturday")
    7 -> println("Sunday")
    else -> println("Invalid day.")
  }
}

// Second day of the week
// Tuesday
```

## Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_when_expression.htm)