---
sidebar_position: 19
---

# Break and Continue
It is used to **exit** a loop once a certain condition is met. 


## Break Statement
If test expression is evaluated to `true`, `break` statment is executed which terminates the loop and program continues to execute just after the loop statment.

```kotlin
// break in for loop
for (...) {
  if (test) {
    break
  } 
}

// break in while loop
while (condition) {
  if (test) {
    break
  } 
}

// break in do while loop
do {
  if (test) {
    break
  } 
} while (condition)
```

The `while` loop terminates when counter variable `i` value becomes 3.
```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  var i = 0;

  // prints i until i is greater than or equal to 100
  while (i++ < 100) {
    println(i)
    // exits the loop when i is equal to 3
    if ( i == 3 ) {
      break
    }
  }
}

// 1
// 2
// 3
```

### Labeled Break Statement

It is the form of `identifier` followed by the `@` sign, for example `test@` or `outer@`. It is used to terminate the specific loop. 

```kotlin
outerLoop@ for (i in 1..100) {
  // ...
}
```

```kotlin
fun main(args: Array<String>) {
  // enters the loop first, then reenters every time it exits the innerLoop
  outerLoop@ for (i in 1..3) {  
    // iterates through 1-3 every time it enters the outerLoop
    innerLoop@ for (j in 1..3) {  
      println("i = $i and j = $j")  
      // exits the loop if i is equal to 2
      if (i == 2) {  
        break@outerLoop
      }  
    }  
  }  
}

// i = 1 and j = 1
// i = 1 and j = 2
// i = 1 and j = 3
// i = 2 and j = 1
```

## Continue Statement
It breaks the loop iteration in between (skips the part next to the continue statement till end of the loop) and continues with the next iteration in the loop.

```kotlin
// Using continue in for loop
for (...) {
  if (test) {
    continue
  } 
}

// Using continue in while loop
while (condition) {
  if (test) {
    continue
  } 
}

// Using continue in do...while loop
do {
  if (test) {
    continue
  } 
} while (condition)
```
If test expression is evaluated to `true`, `continue` statement is executed. It **skips** the remaning part of the loop, and jumps to the next iteration of the loop.

The `while` loop skips printing variable `i` when its value is 3.
```kotlin
fun main(args: Array<String>) {
  // declares an Int variable
  var i = 0;

  // prints i until it's greater than 6 but skips 3
  while (i++ < 6) {
    if( i == 3 ){
      continue
    }
    println(i)
  }
}

// 1
// 2
// 4
// 5
// 6
```

### Labeled Continue Statement
It is used to skip the part of a specific loop. This is done by using continue expression with `@` sign followed by **label** name.

```kotlin
fun main(args: Array<String>) {
  // enters the loop first, then reenters every time it exits the innerLoop
  outerLoop@ for (i in 1..3) {
    // iterates through 1-3 every time it enters the outerLoop
    innerLoop@ for (j in 1..3) { 
      // checks if i is 2 then skips to 3 
      if (i == 2) {  
        continue@outerLoop
      }
      println("i = $i and j = $j")   
    }  
  }  
}

// i = 1 and j = 1
// i = 1 and j = 2
// i = 1 and j = 3
// i = 3 and j = 1
// i = 3 and j = 2
// i = 3 and j = 3
```

## Additional Learning Materials
🔗 [Tutorials Point](https://www.tutorialspoint.com/kotlin/kotlin_break_continue.htm)