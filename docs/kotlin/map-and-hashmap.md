---
sidebar_position: 11
---

# Map and HashMap
**HashMap** is a collection which contains pairs of object. It stores the data in the form of key and value pairs. **Map** keys are unique and the map holds only one value for each key. It is represented as **HashMap<key, value>** or **HashMap<K, V>**.

## Use of HashMap Functions 
Using `HashMap()`, `HashMap(original: Map)`, Traversing hashmap, `HashMap.get()`

```kotlin
fun main(args: Array<String>) {
  // creates empty hashmap
  var hashMap : HashMap<String, Int> = HashMap<String, Int> ()
  
  // checks if the hashmap is empty
  printHashMap(hashMap) 

  // adds elements to the hashMap
  hashMap.put("IronMan" , 3000)
  hashMap.put("Thor" , 100)
  hashMap.put("SpiderMan" , 1100)
  hashMap.put("NickFury" , 1200)
  hashMap.put("HawkEye" , 1300)
  
  // checks if the hashmap is empty
  printHashMap(hashMap)
  
  println("hashMap : " + hashMap + "\n")
  // hashMap : {Thor=100, HawkEye=1300, NickFury=1200, IronMan=3000, SpiderMan=1100}

  // iterates through hashmap keys and prints them
  for(key in hashMap.keys){
    println("Element at key $key : ${hashMap[key]}")
  }
  
  // creates another hashmap object
  var secondHashMap : HashMap<String, Int> = HashMap<String, Int> (hashMap)
  
  println("\n" + "Second HashMap : ")
  // Second HashMap : 

  // iterates through secondHashMap keys and prints them
  for(key in secondHashMap.keys){
    println("Element at key $key : ${secondHashMap.get(key)}")
  }
  
  // clears the first map 
  hashMap.clear()
  
  // prints the updated map
  println("After Clearing : " + hashMap)
  // After Clearing : {}
}
  
fun printHashMap(hashMap : HashMap<String, Int>){
    // checks if the hashmap is empty
    if(hashMap.isEmpty()){
        println("hashMap is empty")
    }else{
        println("hashMap : " + hashMap)
    }
}

// hashMap is empty 
// hashMap : {Thor=100, HawkEye=1300, NickFury=1200, IronMan=3000, SpiderMan=1100}
// hashMap : {Thor=100, HawkEye=1300, NickFury=1200, IronMan=3000, SpiderMan=1100}

// Element at key Thor : 100
// Element at key HawkEye : 1300
// Element at key NickFury : 1200
// Element at key IronMan : 3000
// Element at key SpiderMan : 1100

// Second HashMap : 
// Element at key Thor : 100
// Element at key HawkEye : 1300
// Element at key IronMan : 3000
// Element at key NickFury : 1200
// Element at key SpiderMan : 1100
// After Clearing : {}
```

## Using HashMap initial capacity
Using `HashMap.size`

```kotlin
fun main(args: Array<String>) {
  // creates empty hashmap with initial capacity of 4
  var hashMap : HashMap<String, Int> = HashMap<String, Int> (4)
  
  // adds elements to the hashMap
  hashMap.put("IronMan", 3000)
  hashMap.put("Thor", 100)
  hashMap.put("SpiderMan", 1100)
  hashMap.put("NickFury", 1200)
  
  // iterates through hashMap keys and prints them
  for(key in hashMap.keys) {
    println("Element at key $key : ${hashMap[key]}")
  }

  // returns and prints the size of the hashMap
  println("\n" + "hashMap.size : " + hashMap.size )
  
  // adds a new element to the hashMap
  hashMap["BlackWidow"] = 1000;

  // returns and prints the size of the hashMap
  println("hashMap.size : " + hashMap.size + "\n")
  
  // iterates through hashMap keys and prints them
  for(key in hashMap.keys) {
    println("Element at key $key : ${hashMap[key]}")
  }
}

// Element at key Thor : 100
// Element at key IronMan : 3000
// Element at key NickFury : 1200
// Element at key SpiderMan : 1100

// hashMap.size : 4
// hashMap.size : 5

// Element at key Thor : 100
// Element at key BlackWidow : 1000
// Element at key IronMan : 3000
// Element at key NickFury : 1200
// Element at key SpiderMan : 1100
```

## Other functions 
Using `HashMap.get(key)`, `HashMap.replace()`, `HashMap.put()`

```kotlin
fun main(args: Array<String>) {
  // creates empty hashmap
  var hashMap : HashMap<String, Int> = HashMap<String, Int> ()
  
  // adds elements to the hashMap 
  hashMap.put("IronMan" , 3000)
  hashMap.put("Thor" , 100)
  hashMap.put("SpiderMan" , 1100)
  hashMap.put("Cap" , 1200)
  
  // iterates through hashMap keys and prints them
  for(key in hashMap.keys) {
    println("Element at key $key : ${hashMap[key]}")
  }
  
  println("\nhashMap[\"IronMan\"] : " + hashMap["IronMan"])
  // hashMap["IronMan"] : 3000

  // stores value 2000 to key Thor
  hashMap["Thor"] = 2000

  println("hashMap.get(\"Thor\") : " + hashMap.get("Thor") + "\n")
  // hashMap.get("Thor") : 2000

  // replaces value of key Cap
  hashMap.replace("Cap" , 999)
  
  println("hashMap.replace(\"Cap\" , 999)" + " hashMap.replace(\"Thor\" , 2000)):")
  // hashMap.replace("Cap", 999) hashMap.replace("Thor", 2000)):

  // iterates through hashMap keys and prints them
  for(key in hashMap.keys) {
    println("Element at key $key : ${hashMap[key]}")
  }
}

// Element at key Thor : 100
// Element at key Cap : 1200
// Element at key IronMan : 3000
// Element at key SpiderMan : 1100

// hashMap["IronMan"] : 3000
// hashMap.get("Thor") : 2000

// hashMap.replace("Cap", 999) hashMap.replace("Thor", 2000)):
// Element at key Thor : 2000
// Element at key Cap : 999
// Element at key IronMan : 3000
// Element at key SpiderMan : 1100
```

It provides constant time or O(1) complexity for basic operations like `get` and `put`, if hash function is properly written and disperses the elements properly.

## Additional Learning Materials
🔗 [Geeks for Geeks](https://www.geeksforgeeks.org/kotlin-hashmap/)