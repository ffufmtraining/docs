---
sidebar_position: 13
---

# Flow Control
The statements `if else`, `for`, `when`, `while`, `do while` are **flow control statements**. They determine the next statement to be executed.

## If else
In Kotlin, `if else` operators behave the same way as it does in Java. The `if` clause executes a certain section of code if the condition is `true`, and it can have an optional `else` clause.

## When
It is equivalent to **switch** in other languages, though with a different syntax. If it matches with none, the else statement is printed. The else statement is similar to default in switch.

## For Loop
For loop is used to iterate over a list of items based on certain conditions. The syntax is different from Java. It saves us from declaring the type of the counter variable. The lower and upper limits can be defined on either side of `..` operator. Lastly, `in` keyword is used to iterate over the range. 

```kotlin
// prints 0-5
for (i in 0..5) {
  print(i)
}
```

### forEach Loop
It repeats a set of statements for each iterable.

```kotlin
// prints 2-5
(2..5).forEach{
  println(it)
}

// it can also be written this way

// also prints 2-5
(2..5).forEach{
  i ->  println(i)
}

// 2
// 3
// 4
// 5
```

## While Loop
`while` and `do-while` loops in Kotlin behave the same way as they do in Java.

```kotlin
// declares counter variable
var i = 0

// do while loop, enters the loop at least once before condition is checked
do {
  i += 5 // i = i + 5
  println("Value of i is $i")  // prints the value of i
} while(i < 1) // enters the loop until i is greater than 1

// while loop
while(i <= 5) { // enters the loop until i is greater than 5
  print(i) // prints the value of i
  i++ // increments i
}

// Value of i is 5
// 012345
```

## Break and Continue
The `break` clause is used to **exit** the loop, and `continue` is used to go to the **next** iteration of the loop. We also have the luxury to attach a label to the break and continue statements to indicate the loop on which their actions are triggered.

Proceed to the next topic to get a better grasp on these concepts 😋