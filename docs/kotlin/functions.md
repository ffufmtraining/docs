---
sidebar_position: 20
---

# Functions
They are a group of related statements that perform a specific task. The keyword `fun` is used, which is a shortcut for **function**. They are used to break a large program into smaller and modular chunks. 

:::note
While every method is a function, not every function is a method.
:::

## Types of Functions
Depends on whether a function is defined by the **user**, or available in **standard library.**

### Kotlin Standard Library Function
The standard library functions are built-in functions in Kotlin that are readily available for use. The library function `print()` **prints** message to the standard output stream (monitor). While `sqrt()` returns **square root** of a number (Double value).

```kotlin
fun main(args: Array<String>)   
  var number = 5.5

  print("Result = ${Math.sqrt(number)}") 
  // Result = 2.345207879911715
}
```

### User-defined Functions
As mentioned, you can create functions yourself. Such functions are called **user-defined functions**. In this example, `fun` keyword is used. Followed by the name of the function `callMe` to declare the function.

```kotlin
fun callMe() {
  // function body
}
```

The parenthesis `( )` is empty, because the function doesn't accept any argument. You have to **call** the function to run codes inside the body of the function.
```kotlin
callme()
```

## Adding Two Numbers

```kotlin
fun addNumbers(n1: Double, n2: Double): Int {
  val sum = n1 + n2
  val sumInteger = sum.toInt()
  return sumInteger
}

fun main(args: Array<String>) {
  val number1 = 12.2
  val number2 = 3.4
  val result: Int

  // adds two numbers and converts sum to Int
  result = addNumbers(number1, number2)

  println("result = $result") 
  // result = 15
}
```

##  Arguments and Return Value 
Two arguments `number1` and `number2` of type `Double` are passed to the `addNumbers()` function during function call. These arguments are called **actual arguments**.

```kotlin
result = addNumbers(number1, number2)
```

The parameters `n1` and `n2` accepts the passed arguments. These arguments are called **formal arguments** (or parameters).

```kotlin
fun addNumbers(n1: Double, n2: Double): Int {
  ... ...
  ... ...
}

fun main (args: Array<String>) {
  ... ...
  result = addNumbers(number1, number2)
  ... ...
}
```
Arguments are separated using commas.

:::note 
The data type of actual and formal arguments should match.
:::



In the program, `sumInteger` is returned from `addNumbers()` function. This value is assigned to the variable `result`. This is the **return statement** that terminates the `addNumbers()` function, and control of the program jumps to the `main()` function.

```kotlin
fun addNumbers(n1: Double, n2: Double): Int {
  ... ...
  return sumInteger
}

fun main (args: Array<String>) {
  ... ...
  result = addNumbers(number1, number2)
  ... ...
}
```
Both `sumInteger` and `result` are of type `Int`.

:::note 
The return type of the function is specified in the function definition.
:::

If the function doesn't return any value, its return type is `Unit` and is optional.

## Display Name by Using Function

```kotlin
fun main(args: Array<String>) {
  println(getName("John", "Doe")) 
  // John Doe
}

fun getName(firstName: String, lastName: String): String = "$firstName $lastName"
```

Below, the `getName()` function takes two `String` arguments, and returns a `String`. It is optional to explicitly declare the return type, because it can be inferred by the compiler.

```kotlin
fun getName(firstName: String, lastName: String) = "$firstName $lastName"
```

:::tip
You can omit the curly braces `{ }` if the function returns a single expression 
:::

## Additional Learning Materials
🔗 [Programiz](https://www.programiz.com/kotlin-programming/functions)